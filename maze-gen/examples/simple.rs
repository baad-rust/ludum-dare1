use maze_gen::MazeBuilder;
use tracing::info;
use tracing_subscriber::{fmt, EnvFilter};

fn main() {
    let subscriber = fmt::Subscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        // .pretty()
        .compact()
        .without_time()
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("Failed to initialize the logger.");

    info!("Generating maze");

    let maze = MazeBuilder::new(49, 49, rand::thread_rng())
        .with_num_rooms(100)
        .with_max_room_size(15)
        .with_num_corridors(100)
        .generate();
    println!("{:?}", maze);
}
