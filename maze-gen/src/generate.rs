use rand::{seq::SliceRandom, Rng};
use tracing::trace;

use crate::{Cell, DoorDirection, Maze, Room};

impl Maze {
    /// Generates rooms in the maze.
    pub(crate) fn generate_rooms(
        &mut self,
        num_rooms: usize,
        max_room_size: usize,
        mut rng: impl Rng,
    ) {
        for _ in 0..num_rooms {
            // Calculate the width and height of the room.  It must be odd sized and
            // must be between 3 and `max_room_size`.
            let width = (rng.gen_range(0..max_room_size / 2) * 2 + 1).max(3);
            let height = (rng.gen_range(0..max_room_size / 2) * 2 + 1).max(3);

            // Calculate the top left corner of the room.  It must be even to ensure
            // that the room is aligned with the maze.
            let (x, y) = self.random_location(&mut rng);

            // Calculate the bottom right corner of the room.  Make sure that we clip
            // the room to the maze.
            let x2 = (x + width).min(self.width());
            let y2 = (y + height).min(self.height());

            // Make sure the room is big enough.
            if (x2 - x) < 3 || (y2 - y) < 3 {
                continue;
            }

            // Now we check if the room overlaps with any other rooms.
            let mut overlaps = false;
            for i in x..x2 {
                for j in y..y2 {
                    if self.get(i, j) != Cell::Wall {
                        overlaps = true;
                        break;
                    }
                }
                if overlaps {
                    break;
                }
            }
            if overlaps {
                continue;
            }

            // Carve out the room.
            for i in x..x2 {
                for j in y..y2 {
                    self.set(i, j, Cell::Room(self.region_id));
                }
            }
            let room = Room {
                x,
                y,
                width: x2 - x,
                height: y2 - y,
            };
            self.rooms.push(room);
            self.region_id += 1;
        }
    }

    pub(crate) fn generate_corridors(&mut self, mut rng: impl Rng) {
        for x in (0..self.width()).step_by(2) {
            for y in (0..self.height()).step_by(2) {
                if self.get(x, y) == Cell::Wall {
                    // This is start point for maze generation.
                    self.generate_corridor(x, y, self.region_id, &mut rng);
                    self.region_id += 1;
                }
            }
        }
    }

    fn generate_corridor(
        &mut self,
        mut x: usize,
        mut y: usize,
        corridor_id: usize,
        mut rng: impl Rng,
    ) {
        let mut next_tries = vec![];

        loop {
            // Carve out the initial part of the corridor.
            self.set(x, y, Cell::Corridor(corridor_id));

            // Choose a random direction to move in.
            let mut directions = self.get_directions_of(Cell::Wall, x, y, 2, false);

            // if we have more than one direction to go, we can backtrack.  So let's
            // store this location for later.
            if directions.len() > 1 {
                next_tries.push((x, y));
            }

            if let Some((dx, dy)) = directions.choose(&mut rng) {
                // Carve out the corridor.
                self.set(
                    ((x as i32) + dx) as usize,
                    ((y as i32) + dy) as usize,
                    Cell::Corridor(corridor_id),
                );
                self.set(
                    ((x as i32) + dx * 2) as usize,
                    ((y as i32) + dy * 2) as usize,
                    Cell::Corridor(corridor_id),
                );

                // Move to the next cell.
                x = (x as i32 + (dx * 2)) as usize;
                y = (y as i32 + (dy * 2)) as usize;
            } else {
                // No more directions so we are done with this particular corridor.
                // Let's first try to backtrack then we can start a new corridor.
                if let Some((nx, ny)) = next_tries.pop() {
                    x = nx;
                    y = ny;
                } else {
                    // No more backtracking so we are done.
                    break;
                }
            }
        }
    }

    fn get_directions_of(
        &mut self,
        cell: Cell,
        x: usize,
        y: usize,
        distance: usize,
        include_out_of_bounds: bool,
    ) -> Vec<(i32, i32)> {
        let mut directions = vec![];

        assert!(distance <= 2, "distance must be less than or equal to 2");

        // Test left.
        if x >= 2 {
            if self.get(x - distance, y).is_same_type(&cell) {
                directions.push((-1, 0));
            }
        } else if include_out_of_bounds && cell == Cell::Wall {
            directions.push((-1, 0));
        }

        // Test right.
        if x < self.width() - 2 {
            if self.get(x + distance, y).is_same_type(&cell) {
                directions.push((1, 0));
            }
        } else if include_out_of_bounds && cell == Cell::Wall {
            directions.push((1, 0));
        }

        // Test up.
        if y >= 2 {
            if self.get(x, y - distance).is_same_type(&cell) {
                directions.push((0, -1));
            }
        } else if include_out_of_bounds && cell == Cell::Wall {
            directions.push((0, -1));
        }

        // Test down.
        if y < self.height() - 2 {
            if self.get(x, y + distance).is_same_type(&cell) {
                directions.push((0, 1));
            }
        } else if include_out_of_bounds && cell == Cell::Wall {
            directions.push((0, 1));
        }

        directions
    }

    pub(crate) fn shrink_corridors(&mut self, min_num_corridors: usize) {
        // First figure out where are all the dead ends in the maze.  These will be
        // cells that are corridors but are surrounded by 3 walls.
        let mut dead_ends = vec![];
        let mut num_corridors_cells = 0;

        for x in (0..self.width()).step_by(2) {
            for y in (0..self.height()).step_by(2) {
                if self.get(x, y).is_corridor() {
                    let wall_directions = self.get_directions_of(Cell::Wall, x, y, 1, true);

                    if wall_directions.len() == 3 {
                        dead_ends.push((x, y));
                    }

                    num_corridors_cells += 1;
                }
            }
        }

        let mut dead_end_index = 0;

        while num_corridors_cells > min_num_corridors {
            if dead_end_index == dead_ends.len() {
                break;
            }

            let (x, y) = dead_ends[dead_end_index];

            // We have a dead end, let's remove it.
            let Cell::Corridor(corridor_id) = self.get(x, y) else {
                panic!("Expected a corridor cell at ({}, {})", x, y);
            };
            self.set(x, y, Cell::Wall);
            num_corridors_cells -= 1;

            // Now we need to figure out which way from the dead end the
            // corridor went so we can possibly register a new dead end.
            let corridor_directions =
                self.get_directions_of(Cell::Corridor(corridor_id), x, y, 1, false);
            assert!(corridor_directions.len() <= 1);

            if let Some((dx, dy)) = corridor_directions.first() {
                let nx = (x as i32 + dx) as usize;
                let ny = (y as i32 + dy) as usize;

                // If the next cell is a corridor, we can register it as a new
                // dead end.
                let wall_directions = self.get_directions_of(Cell::Wall, nx, ny, 1, true);

                if wall_directions.len() == 3 {
                    dead_ends.push((nx, ny));
                }
            }

            // Move to the next dead end.
            dead_end_index += 1;
        }
    }

    fn generate_connections(&mut self) -> Vec<Connection> {
        let mut connections = vec![];

        // Find all the connections between corridors and rooms in the self.  A
        // connection will be a wall that is surrounded by 2 sets of either room or
        // corridor from different regions.
        for x in (0..self.width() - 1) {
            for y in (0..self.height() - 1) {
                if self.get(x, y).is_wall() {
                    // Check to see if the surrounding horizontal cells are connectable.
                    if x > 0
                        && x < self.width() - 1
                        && is_connectable(self.get(x - 1, y), self.get(x + 1, y))
                    {
                        connections.push(Connection {
                            from: (x - 1, y),
                            to: (x + 1, y),
                        });
                    }

                    // Check to see if the surrounding vertical cells are connectable.
                    if y > 0
                        && y < self.height() - 1
                        && is_connectable(self.get(x, y - 1), self.get(x, y + 1))
                    {
                        connections.push(Connection {
                            from: (x, y - 1),
                            to: (x, y + 1),
                        });
                    }
                }
            }
        }

        connections
    }

    pub(crate) fn generate_doors(&mut self, mut rng: impl Rng) {
        // Get a list of all the connections in the maze.
        let mut connections = self.generate_connections();

        while let Some(connection) = connections.choose(&mut rng) {
            // Now we generate a door cell in the middle of the connection.
            let door_cell = generate_connection_cell(connection);
            self.set(door_cell.1, door_cell.2, door_cell.0);

            // Now we merge the two regions together.
            let new_region_id = self.region_id;
            self.region_id += 1;

            self.merge_regions(connection.from.0, connection.from.1, new_region_id);

            // Remove all connections that have both regions in them.
            let (match_region1, match_region2) = connection.get_regions(self);
            connections = connections
                .into_iter()
                .filter(|c| {
                    let (region1, region2) = c.get_regions(self);
                    let matching_regions = (region1 == match_region1 && region2 == match_region2)
                        || (region1 == match_region2 && region2 == match_region1);
                    !matching_regions
                })
                .collect::<Vec<_>>();
        }
    }

    fn merge_regions(&mut self, region1: usize, region2: usize, new_region_id: usize) {
        for x in 0..self.width() {
            for y in 0..self.height() {
                let cell = self.get(x, y);
                match cell {
                    Cell::Room(region) if region == region1 || region == region2 => {
                        self.set(x, y, Cell::Room(new_region_id));
                    }
                    Cell::Corridor(region) if region == region1 || region == region2 => {
                        self.set(x, y, Cell::Corridor(new_region_id));
                    }
                    _ => {}
                }
            }
        }
    }

    pub(crate) fn remove_orphaned_doors(&mut self) {
        for x in 0..self.width() {
            for y in 0..self.height() {
                if let Cell::Door(direction) = self.get(x, y) {
                    let mut is_orphaned = true;

                    // Check to see if the door is connected to a corridor.
                    match direction {
                        DoorDirection::Vertical => {
                            if x > 0
                                && x < self.width() - 1
                                && self.get(x - 1, y).is_floor()
                                && self.get(x + 1, y).is_floor()
                            {
                                is_orphaned = false;
                            }
                        }
                        DoorDirection::Horizontal => {
                            if y > 0
                                && y < self.height() - 1
                                && self.get(x, y - 1).is_floor()
                                && self.get(x, y + 1).is_floor()
                            {
                                is_orphaned = false;
                            }
                        }
                    }

                    if is_orphaned {
                        self.set(x, y, Cell::Wall);
                    }
                }
            }
        }
    }
}

struct Connection {
    from: (usize, usize),
    to: (usize, usize),
}

impl Connection {
    fn get_regions(&self, maze: &Maze) -> (usize, usize) {
        let region1 = match maze.get(self.from.0, self.from.1) {
            Cell::Room(region) => region,
            Cell::Corridor(region) => region,
            _ => panic!("Expected a room or corridor cell"),
        };

        let region2 = match maze.get(self.to.0, self.to.1) {
            Cell::Room(region) => region,
            Cell::Corridor(region) => region,
            _ => panic!("Expected a room or corridor cell"),
        };

        (region1, region2)
    }
}

fn is_connectable(cell1: Cell, cell2: Cell) -> bool {
    match (cell1, cell2) {
        (Cell::Room(_), Cell::Corridor(_)) => true,
        (Cell::Corridor(_), Cell::Room(_)) => true,
        (Cell::Room(r1), Cell::Room(r2)) => r1 != r2,
        (Cell::Corridor(c1), Cell::Corridor(c2)) => c1 != c2,
        _ => false,
    }
}

fn generate_connection_cell(connection: &Connection) -> (Cell, usize, usize) {
    let (x, y) = (
        (connection.from.0 + connection.to.0) / 2,
        (connection.from.1 + connection.to.1) / 2,
    );
    (
        Cell::Door(if connection.from.0 == connection.to.0 {
            DoorDirection::Horizontal
        } else {
            DoorDirection::Vertical
        }),
        x,
        y,
    )
}
