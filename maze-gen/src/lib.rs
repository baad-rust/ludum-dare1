#![allow(unused)]

mod generate;

use std::fmt::{Debug, Formatter, Result};

use rand::{seq::index, Rng};

pub struct MazeBuilder<R: Rng> {
    width: usize,
    height: usize,
    num_rooms: usize,
    max_room_size: usize,
    num_corridors: usize,
    rng: R,
}

impl<R: Rng> MazeBuilder<R> {
    /// Create a new maze builder with the given width and height.  The width
    /// and height must be odd numbers.
    pub fn new(width: usize, height: usize, rng: R) -> MazeBuilder<R> {
        assert!(
            width % 2 == 1 && height % 2 == 1,
            "Width and height must be odd"
        );
        MazeBuilder {
            width,
            height,
            num_rooms: (width * height / 100),
            max_room_size: 9,
            num_corridors: (width * height) / 8,
            rng,
        }
    }

    /// Set the number of rooms to attempt to generate in the maze.  It is not
    /// guaranteed that we will reach this number after generation.
    pub fn with_num_rooms(mut self, num_rooms: usize) -> MazeBuilder<R> {
        self.num_rooms = num_rooms;
        self
    }

    /// Set the maximum size of a room.  The width and height of a room will be
    /// between 3 and `max_room_size`.
    pub fn with_max_room_size(mut self, max_room_size: usize) -> MazeBuilder<R> {
        assert!(max_room_size >= 3, "Max room size must be at least 3");
        assert!(max_room_size % 2 == 1, "Max room size must be odd");
        self.max_room_size = max_room_size;
        self
    }

    /// Set the number of corrdior cells to leave behind after generating the
    /// initial corrdiors between the rooms.
    pub fn with_num_corridors(mut self, num_corridors: usize) -> MazeBuilder<R> {
        self.num_corridors = num_corridors;
        self
    }

    /// Finally generate the maze.
    pub fn generate(mut self) -> Maze {
        let mut maze = Maze::new(self.width, self.height);

        maze.generate_rooms(self.num_rooms, self.max_room_size, &mut self.rng);
        maze.generate_corridors(&mut self.rng);
        maze.generate_doors(&mut self.rng);
        maze.shrink_corridors(self.num_corridors);
        maze.remove_orphaned_doors();

        maze
    }
}

/// A maze is a grid of cells.  Each cell is of type `Cell`.
pub struct Maze {
    /// The width of the maze.
    width: usize,

    /// The latest region id.
    region_id: usize,

    /// The cells of the maze.  The cells are stored in row-major order.
    cells: Vec<Cell>,

    /// The room regions of the maze.
    rooms: Vec<Room>,
}

impl Debug for Maze {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let mut output = String::new();
        let border = "\u{2588}".repeat(self.width + 2);
        output.push_str(&format!("{border}\n"));
        self.cells.chunks(self.width).for_each(|row| {
            output.push('\u{2588}');
            row.iter().for_each(|cell| {
                output.push_str(&format!("{:?}", cell));
            });
            output.push('\u{2588}');
            output.push('\n');
        });
        output.push_str(&format!("{border}\n"));
        write!(f, "{}", output)
    }
}

#[derive(Debug)]
pub struct Room {
    pub x: usize,
    pub y: usize,
    pub width: usize,
    pub height: usize,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Cell {
    /// A wall cell.
    Wall,

    /// A corridor cell.  The corridor number is stored in the cell.
    Corridor(usize),

    /// A room cell.  The room number is stored in the cell.
    Room(usize),

    // A door cell.  The door direction is stored in the cell.
    Door(DoorDirection),
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum DoorDirection {
    Horizontal,
    Vertical,
}

impl Cell {
    pub fn is_wall(&self) -> bool {
        matches!(self, Cell::Wall)
    }

    pub fn is_corridor(&self) -> bool {
        matches!(self, Cell::Corridor(_))
    }

    pub fn is_room(&self) -> bool {
        matches!(self, Cell::Room(_))
    }

    pub fn is_door(&self) -> bool {
        matches!(self, Cell::Door(_))
    }

    pub fn is_floor(&self) -> bool {
        matches!(self, Cell::Corridor(_) | Cell::Room(_) | Cell::Door(_))
    }

    /// Returns true if the cell enum part is the same but ignores any attached
    /// data.
    pub fn is_same_type(&self, other: &Cell) -> bool {
        matches!(
            (self, other),
            (Cell::Wall, Cell::Wall)
                | (Cell::Corridor(_), Cell::Corridor(_))
                | (Cell::Room(_), Cell::Room(_))
                | (Cell::Door(_), Cell::Door(_))
        )
    }
}

impl Debug for Cell {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let c = match self {
            Cell::Wall => '\u{2588}',
            Cell::Corridor(_) => ' ',
            Cell::Room(_) => '.',
            // Cell::Connection => '\u{2591}',
            Cell::Door(DoorDirection::Horizontal) => '\u{2500}',
            Cell::Door(DoorDirection::Vertical) => '\u{2502}',
        };
        write!(f, "{}", c)
    }
}

impl Maze {
    /// Create a new maze with the given width and height.  This is called by
    /// `MazeBuilder::generate()` and should not be called directly.
    fn new(width: usize, height: usize) -> Maze {
        let cells = vec![Cell::Wall; width * height];
        Maze {
            width,
            region_id: 0,
            cells,
            rooms: Vec::new(),
        }
    }

    /// Get the width of the maze.
    pub fn width(&self) -> usize {
        self.width
    }

    /// Get the height of the maze.
    pub fn height(&self) -> usize {
        self.cells.len() / self.width
    }

    /// Return a random location in the maze.  The coordinates must be even.
    pub fn random_location<R: Rng>(&self, mut rng: R) -> (usize, usize) {
        let x = rng.gen_range(0..self.width / 2) * 2;
        let y = rng.gen_range(0..self.height() / 2) * 2;

        (x, y)
    }

    pub fn get(&self, x: usize, y: usize) -> Cell {
        self.cells[y * self.width + x]
    }

    pub fn set(&mut self, x: usize, y: usize, cell: Cell) {
        self.cells[y * self.width + x] = cell;
    }

    pub fn room_count(&self) -> usize {
        self.rooms.len()
    }

    pub fn get_room(&self, index: usize) -> Option<&Room> {
        self.rooms.get(index)
    }

    pub fn get_room_centre(&self, index: usize) -> Option<(usize, usize)> {
        if let Some(room) = self.rooms.get(index) {
            Some((room.x + room.width / 2, room.y + room.height / 2))
        } else {
            None
        }
    }

    pub fn get_primary_room_id(&self) -> usize {
        0
    }

    pub fn get_primary_pos(&self) -> (usize, (usize, usize)) {
        let room = self
            .rooms
            .first()
            .expect("There has to be at least one room!");
        (
            self.get_primary_room_id(),
            (room.x + room.width / 2, room.y + room.height / 2),
        )
    }
}
