// #![allow(unused)]
mod dungeon;
mod game;
mod gamestate;
mod hunter;
mod pathfinding;
mod pedestal;
mod player;

use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;
use game::{GamePlugins, TilemapMetadata};
use hunter::{
    check_if_player_in_awareness_radius, hunter_check_last_known_position, hunter_find_path,
    hunter_search_for_player, move_hunter, spawn_hunter, HunterFindPathEvent, HunterSpawnEvent,
};
use pedestal::{
    activate_pedestals, check_for_rune_collection, despawn_collected_runes, spawn_pedestals,
    spawn_runes, PedestalSpawnEvent, PlayerInventory, RuneDespawnEvent, RuneSpawnEvent,
};
use player::{camera_follow_player, player_move, spawn_player, PlayerSpawnEvent};

#[derive(Resource)]
struct TilemapMetadataHandle {
    pub handle: Handle<TilemapMetadata>,
    pub tileset_name: String,
}

#[derive(Resource, Default)]
struct LoadingCheckList {
    tile_metadata: bool,
    key_mapping: bool,
}

impl LoadingCheckList {
    fn all_complete(&self) -> bool {
        self.tile_metadata && self.key_mapping
    }
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Hash, States)]
enum AppState {
    #[default]
    Loading,
    LevelInit,
    Running,
}

#[derive(States, Default, Debug, Hash, Eq, PartialEq, Clone)]
enum CameraState {
    #[default]
    FollowPlayer,
    DetachedCamera,
}

fn main() {
    App::new()
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: String::from("He has the time!"),
                        ..Default::default()
                    }),
                    ..Default::default()
                })
                .set(ImagePlugin::default_nearest()),
        )
        .add_plugins(TilemapPlugin)
        .add_plugins(GamePlugins)
        .add_event::<PlayerSpawnEvent>()
        .add_event::<HunterSpawnEvent>()
        .add_event::<HunterFindPathEvent>()
        .add_event::<PedestalSpawnEvent>()
        .add_event::<RuneSpawnEvent>()
        .add_event::<RuneDespawnEvent>()
        .insert_state(CameraState::default())
        .insert_resource(PlayerInventory::default())
        .add_systems(
            Update,
            (
                spawn_player,
                spawn_hunter,
                spawn_pedestals,
                spawn_runes,
                (
                    check_if_player_in_awareness_radius,
                    hunter_search_for_player,
                    hunter_find_path,
                    hunter_check_last_known_position,
                )
                    .chain(),
                move_hunter,
                check_for_rune_collection,
                despawn_collected_runes,
                activate_pedestals,
                (player_move, camera_follow_player).run_if(in_state(CameraState::FollowPlayer)),
            )
                .run_if(in_state(AppState::Running)),
        )
        .add_systems(Update, bevy::window::close_on_esc)
        .run();
}
