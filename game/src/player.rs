use bevy::prelude::*;
use bevy_ecs_tilemap::tiles::{TilePos, TileStorage};

use crate::game::{ChunkCoords, ChunkTracking, ControlState, KeyAction, Traversability};

#[derive(Event)]
pub struct PlayerSpawnEvent {
    pub spawn_coords: ChunkCoords,
}

#[derive(Component)]
pub struct CameraFocalRect {
    pub size: Vec2,
}

#[derive(Component)]
pub struct SpawnPlayer;

pub enum FacingDirection {
    North,
    South,
    East,
    West,
}

#[derive(Component)]
pub struct Player {
    pub chunk_coords: ChunkCoords,
    pub speed_tiles_per_second: f32,
    pub between_tiles: bool,
    pub facing: FacingDirection,
}

impl FacingDirection {
    pub fn as_euler(&self) -> f32 {
        match self {
            FacingDirection::North => 0.,
            FacingDirection::South => std::f32::consts::PI,
            FacingDirection::East => std::f32::consts::PI * 1.5,
            FacingDirection::West => std::f32::consts::PI / 2.,
        }
    }

    pub fn as_quat(&self) -> Quat {
        Quat::from_rotation_z(self.as_euler())
    }
}

impl Player {
    pub fn calculate_resulting_coords(
        &self,
        chunk_tracking: &ChunkTracking,
        movement: &IVec2,
    ) -> ChunkCoords {
        chunk_tracking.calculate_resulting_coords_from_movement(&self.chunk_coords, movement)
    }
}

pub fn spawn_player(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    chunk_tracking: Res<ChunkTracking>,
    mut events: EventReader<PlayerSpawnEvent>,
    mut camera_query: Query<&mut Transform, With<Camera2d>>,
) {
    for event in events.read() {
        let spawning_chunk_coords = &event.spawn_coords;
        let world_pos = chunk_tracking.calculate_world_pos(spawning_chunk_coords);
        commands
            .spawn(SpriteBundle {
                texture: asset_server.load("player_placeholder.png"),
                transform: Transform::from_translation(world_pos.extend(1.0)),
                ..Default::default()
            })
            .insert(Player {
                chunk_coords: spawning_chunk_coords.clone(),
                speed_tiles_per_second: 20.0,
                between_tiles: false,
                facing: FacingDirection::North,
            });

        if let Some(mut camera) = camera_query.iter_mut().next() {
            camera.translation = world_pos.extend(camera.translation.z);
        }
        // only one player
        break;
    }

    events.clear();
}

pub fn player_move(
    time: Res<Time>,
    controls: Res<ControlState>,
    mut player_query: Query<(&mut Player, &mut Transform)>,
    chunk_query: Query<(Entity, &Transform, &TileStorage), Without<Player>>,
    tile_query: Query<&Traversability>,
    chunk_tracking: Res<ChunkTracking>,
) {
    for (mut player, mut transform) in player_query.iter_mut() {
        if player.between_tiles {
            let delta = time.delta_seconds();
            let relative_pos_this_frame = player
                .chunk_coords
                .tile_relative_pos
                .lerp(Vec2::ZERO, delta * player.speed_tiles_per_second);
            let relative_pos_this_frame = if relative_pos_this_frame.length() < 0.25 {
                player.between_tiles = false;
                Vec2::ZERO
            } else {
                relative_pos_this_frame
            };

            player.chunk_coords.tile_relative_pos = relative_pos_this_frame;

            let world_pos = chunk_tracking.calculate_world_pos(&player.chunk_coords);
            transform.translation = world_pos.extend(1.0);
            continue;
        }

        let mut direction = IVec2::ZERO;

        if controls.0.state[KeyAction::MoveUp].pressed() {
            direction.y += 1;
        }

        if controls.0.state[KeyAction::MoveDown].pressed() {
            direction.y -= 1;
        }

        if controls.0.state[KeyAction::MoveLeft].pressed() {
            direction.x -= 1;
        }

        if controls.0.state[KeyAction::MoveRight].pressed() {
            direction.x += 1;
        }

        if direction.x > 0 {
            player.facing = FacingDirection::East;
        } else if direction.x < 0 {
            player.facing = FacingDirection::West;
        } else if direction.y > 0 {
            player.facing = FacingDirection::North;
        } else if direction.y < 0 {
            player.facing = FacingDirection::South;
        }

        if direction == IVec2::ZERO {
            continue;
        }

        let desired_chunk_coords = player.calculate_resulting_coords(&chunk_tracking, &direction);

        // check desired coord is a valid tile
        if let Some((_, _, tilemap_storage)) = chunk_query.iter().find(|(_, chunk_transform, _)| {
            let chunk_pos = chunk_transform.translation.xy();
            let chunk_coord = chunk_tracking.world_pos_to_chunk_pos(&chunk_pos);
            desired_chunk_coords.chunk_coord == chunk_coord
        }) {
            if let Some(tile_entity) = tilemap_storage.get(&TilePos {
                x: desired_chunk_coords.chunk_relative_coord.x,
                y: desired_chunk_coords.chunk_relative_coord.y,
            }) {
                if let Ok(Traversability::Passable) = tile_query.get(tile_entity) {
                    // write chunk coords back to player
                    player.chunk_coords = desired_chunk_coords;
                    player.between_tiles = true;

                    let world_pos = chunk_tracking.calculate_world_pos(&player.chunk_coords);

                    transform.translation = world_pos.extend(1.0);
                }
            }
        }

        transform.rotation = player.facing.as_quat();
    }
}

#[allow(clippy::type_complexity)]
pub fn camera_follow_player(
    time: Res<Time>,
    query: Query<(&Transform, &Player), With<Player>>,
    controls: Res<ControlState>,
    mut camera_query: Query<
        (
            &mut Transform,
            &mut OrthographicProjection,
            &CameraFocalRect,
        ),
        (With<Camera2d>, Without<Player>),
    >,
) {
    // Use the camera's focal rect to limit camera movement so that it responds only if the player
    // moves outside of the area of focus that the camera is configured with
    // when following the player do not snap to the player's position, instead snap to a position such
    // that the player is at the edge of the focal rect
    for (player_transform, player) in query.iter() {
        for (mut camera_transform, mut ortho, focal_rect) in camera_query.iter_mut() {
            let player_pos = player_transform.translation.xy();

            let player_distance_from_focal_center = player_pos - camera_transform.translation.xy();
            if player_distance_from_focal_center.x.abs() > focal_rect.size.x / 2.0
                || player_distance_from_focal_center.y.abs() > focal_rect.size.y / 2.0
            {
                let direction = player_distance_from_focal_center.normalize();
                let new_camera_pos =
                    camera_transform.translation.xy() + (direction * (focal_rect.size / 16.0));
                camera_transform.translation = camera_transform
                    .translation
                    .xy()
                    .lerp(
                        new_camera_pos,
                        player.speed_tiles_per_second * time.delta_seconds(),
                    )
                    .extend(camera_transform.translation.z);
            }

            // handle zoom in/out
            if controls.0.state[KeyAction::ZoomIn].pressed() {
                ortho.scale -= 0.1;
            }
            if controls.0.state[KeyAction::ZoomOut].pressed() {
                ortho.scale += 0.1;
            }

            ortho.scale = ortho.scale.clamp(0.5, f32::INFINITY);
        }
    }
}
