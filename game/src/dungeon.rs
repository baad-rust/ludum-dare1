use bevy::prelude::*;
use maze_gen::{Maze, MazeBuilder};
use rand::Rng;

use crate::pedestal::Rune;

#[derive(Debug, Resource)]
pub struct Dungeon {
    pub maze: Maze,
    pub player_spawn_room_id: usize,
    pub player_spawn: (usize, usize),
    pub pedestal_spawns: Vec<(usize, usize, Rune)>,
    pub rune_spawns: Vec<(usize, usize, Rune)>,
}

impl Dungeon {
    pub fn new<R: Rng + Clone>(mut rng: R) -> Self {
        let maze = MazeBuilder::new(49, 49, rng.clone())
            .with_num_rooms(100)
            .with_max_room_size(15)
            .with_num_corridors(100)
            .generate();

        let (player_spawn_room_id, player_spawn) = maze.get_primary_pos();

        let runes = vec![Rune::H, Rune::O, Rune::R, Rune::N];
        let pedestal_spawns = runes
            .iter()
            .enumerate()
            .map(|(i, rune)| {
                let horizontal_sign = if i % 2 == 0 { 1 } else { -1 };
                let vertical_sign = if i / 2 == 0 { 1 } else { -1 };
                let x = player_spawn.0 as i32 + horizontal_sign;
                let y = player_spawn.1 as i32 + vertical_sign;
                (x as usize, y as usize, *rune)
            })
            .collect();

        let room_count = maze.room_count();
        let mut occupied_rooms = vec![player_spawn_room_id];
        let rune_spawns = runes
            .iter()
            .map(|rune| {
                let mut room = player_spawn_room_id;
                while occupied_rooms.contains(&room) {
                    room = rng.gen_range(0..room_count);
                }

                occupied_rooms.push(room);
                let room = maze.get_room(room).expect("Room out of range");

                let x_in_room = rng.gen_range(0..room.width);
                let y_in_room = rng.gen_range(0..room.height);

                (room.x + x_in_room, room.y + y_in_room, *rune)
            })
            .collect();

        Self {
            maze,
            player_spawn_room_id,
            player_spawn,
            pedestal_spawns,
            rune_spawns,
        }
    }
}
