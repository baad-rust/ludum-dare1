use std::time::Duration;

use bevy::prelude::*;
use bevy_ecs_tilemap::{map::TilemapSize, tiles::TilePos};
use rand::Rng;

use crate::{
    dungeon::Dungeon,
    game::{ChunkCoords, ChunkTracking, Rand},
    pathfinding::{find_path, Path},
    player::{FacingDirection, Player},
};

#[derive(Event)]
pub struct HunterSpawnEvent;

#[derive(Event)]
pub struct HunterFindPathEvent {
    maze_coords: (usize, usize),
}

#[derive(Component)]
pub struct HunterSpawnMarker {
    pub spawn_bias: f32,
}

#[derive(Component)]
pub struct Hunter {
    pub last_known_position_valid_timer: Timer,
    pub last_known_player_position: Option<(usize, usize)>,
    pub current_path: Option<Path>,
    pub awarenes_radius: u32,
    pub chunk_coords: ChunkCoords,
    pub chase_speed_tiles_per_second: f32,
    pub search_speed_tiles_per_second: f32,
    pub between_tiles: bool,
    pub facing: FacingDirection,
}

impl HunterSpawnMarker {
    pub fn new(spawn_bias: f32) -> Self {
        Self { spawn_bias }
    }
}

pub fn spawn_hunter(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut rng: ResMut<Rand>,
    mut events: EventReader<HunterSpawnEvent>,
    chunks: Query<&Transform, With<TilemapSize>>,
    tiles: Query<(&Parent, &TilePos, &HunterSpawnMarker)>,
    chunk_tracking: Res<ChunkTracking>,
) {
    if events.is_empty() {
        return;
    }

    let count = tiles.iter().count();
    for (i, (chunk, tile_pos, spawn_marker)) in tiles.iter().enumerate() {
        let random_value: f32 = rng.gen();
        if random_value < spawn_marker.spawn_bias && i < count - 1 {
            continue;
        }

        let chunk_transform = chunks.get(chunk.get()).unwrap();
        let chunk_coords = ChunkCoords {
            chunk_coord: chunk_tracking.world_pos_to_chunk_pos(&chunk_transform.translation.xy()),
            chunk_relative_coord: UVec2 {
                x: tile_pos.x as u32,
                y: tile_pos.y as u32,
            },
            tile_relative_pos: Vec2::ZERO,
        };

        let world_pos = chunk_tracking.calculate_world_pos(&chunk_coords);
        let mut timer = Timer::from_seconds(5.0, TimerMode::Once);
        timer.set_elapsed(Duration::from_secs_f32(5.0));
        commands.spawn((
            SpriteBundle {
                texture: asset_server.load("hunter_placeholder.png"),
                transform: Transform::from_translation(world_pos.extend(1.0)),
                ..Default::default()
            },
            Hunter {
                last_known_position_valid_timer: timer,
                last_known_player_position: None,
                current_path: None,
                awarenes_radius: 5,
                chunk_coords: chunk_coords,
                chase_speed_tiles_per_second: 20.0,
                search_speed_tiles_per_second: 15.0,
                between_tiles: false,
                facing: FacingDirection::North,
            },
        ));
        break;
    }

    events.clear();
}

impl Hunter {
    pub fn calculate_path_to_chunk_coords(
        &self,
        target_chunk_coords: &ChunkCoords,
        dungeon: &Dungeon,
        chunk_tracking: &ChunkTracking,
    ) -> Option<Path> {
        let end = chunk_tracking.chunk_coords_to_maze_coords(target_chunk_coords);
        let path = self.calculate_path_to_maze_coords(end, dungeon, chunk_tracking);
        path
    }

    pub fn calculate_path_to_maze_coords(
        &self,
        target_maze_coords: (usize, usize),
        dungeon: &Dungeon,
        chunk_tracking: &ChunkTracking,
    ) -> Option<Path> {
        let start = chunk_tracking.chunk_coords_to_maze_coords(&self.chunk_coords);
        let path = find_path(&dungeon.maze, start, target_maze_coords);
        path
    }
}

pub fn check_if_player_in_awareness_radius(
    mut hunter_query: Query<&mut Hunter>,
    player_query: Query<&Player>,
    chunk_tracking: Res<ChunkTracking>,
) {
    for mut hunter in hunter_query.iter_mut() {
        if let Some(player) = player_query.iter().next() {
            let (player_x, player_y) =
                chunk_tracking.chunk_coords_to_maze_coords(&player.chunk_coords);

            if let Some(last_known_player_maze_coord) = hunter.last_known_player_position {
                if last_known_player_maze_coord == (player_x, player_y) {
                    continue;
                }
            }

            let (hunter_x, hunter_y) =
                chunk_tracking.chunk_coords_to_maze_coords(&hunter.chunk_coords);

            let player_coords = IVec2::new(player_x as i32, player_y as i32);
            let hunter_coords = IVec2::new(hunter_x as i32, hunter_y as i32);

            let distance = player_coords.distance_squared(hunter_coords);
            if distance <= (hunter.awarenes_radius * hunter.awarenes_radius) as i32 {
                hunter.last_known_player_position = Some((player_x, player_y));
                hunter.current_path = None; // treat existing path as stale, another will be calculated
                hunter.last_known_position_valid_timer.reset();
            }
        }
    }
}

pub fn hunter_search_for_player(
    hunter_query: Query<&Hunter>,
    dungeon: Res<Dungeon>,
    mut rng: ResMut<Rand>,
    mut path_events: EventWriter<HunterFindPathEvent>,
) {
    for hunter in hunter_query.iter() {
        // already have a path, continue
        if hunter.current_path.is_some() {
            continue;
        }

        // path to the last known player position if we have one
        if let Some(last_known_player_maze_coord) = hunter.last_known_player_position {
            path_events.send(HunterFindPathEvent {
                maze_coords: last_known_player_maze_coord,
            });
            continue;
        }

        // pick a random point in another room and path to it
        let primary_room = dungeon.maze.get_primary_room_id();
        let room_count = dungeon.maze.room_count();
        let mut random_room_index = primary_room;
        while random_room_index == primary_room {
            random_room_index = rng.gen_range(0..room_count);
        }

        if let Some(room_centre) = dungeon.maze.get_room_centre(random_room_index) {
            path_events.send(HunterFindPathEvent {
                maze_coords: room_centre,
            });
        }
    }
}

pub fn hunter_find_path(
    mut query: Query<&mut Hunter>,
    dungeon: Res<Dungeon>,
    chunk_tracking: Res<ChunkTracking>,
    mut path_events: EventReader<HunterFindPathEvent>,
) {
    for event in path_events.read() {
        for mut hunter in query.iter_mut() {
            let path =
                hunter.calculate_path_to_maze_coords(event.maze_coords, &dungeon, &chunk_tracking);
            if let Some(path) = path {
                hunter.current_path = Some(path);
            }
        }
    }
}

pub fn hunter_check_last_known_position(time: Res<Time>, mut query: Query<&mut Hunter>) {
    for mut hunter in query.iter_mut() {
        hunter.last_known_position_valid_timer.tick(time.delta());
        if hunter.last_known_position_valid_timer.just_finished() {
            hunter.last_known_player_position = None;
        }
    }
}

#[allow(dead_code)]
pub fn calculate_path_to_player(
    mut query: Query<&mut Hunter>,
    player_query: Query<&Player>,
    dungeon: Res<Dungeon>,
    chunk_tracking: Res<ChunkTracking>,
) {
    for mut hunter in query.iter_mut() {
        if hunter.current_path.is_some() {
            continue;
        }

        if let Some(player) = player_query.iter().next() {
            let path = hunter.calculate_path_to_chunk_coords(
                &player.chunk_coords,
                &dungeon,
                &chunk_tracking,
            );
            hunter.current_path = path;
            hunter.last_known_player_position =
                Some(chunk_tracking.chunk_coords_to_maze_coords(&player.chunk_coords));
        }
    }
}

pub fn move_hunter(
    time: Res<Time>,
    mut query: Query<(&mut Transform, &mut Hunter)>,
    chunk_tracking: Res<ChunkTracking>,
) {
    for (mut transform, mut hunter) in query.iter_mut() {
        if hunter.between_tiles {
            let speed_tiles_per_second = if hunter.last_known_player_position.is_some() {
                hunter.chase_speed_tiles_per_second
            } else {
                hunter.search_speed_tiles_per_second
            };
            let delta = time.delta_seconds();
            let relative_pos_this_frame = hunter
                .chunk_coords
                .tile_relative_pos
                .lerp(Vec2::ZERO, delta * speed_tiles_per_second);
            let relative_pos_this_frame = if relative_pos_this_frame.length() < 0.25 {
                let current_maze_coords =
                    chunk_tracking.chunk_coords_to_maze_coords(&hunter.chunk_coords);
                if let Some(last_known_player_maze_coord) = hunter.last_known_player_position {
                    if current_maze_coords == last_known_player_maze_coord {
                        hunter.current_path = None;
                    }
                }

                hunter.between_tiles = false;
                Vec2::ZERO
            } else {
                relative_pos_this_frame
            };

            hunter.chunk_coords.tile_relative_pos = relative_pos_this_frame;
            let world_pos = chunk_tracking.calculate_world_pos(&hunter.chunk_coords);
            transform.translation = world_pos.extend(1.0);
            continue;
        }

        if let Some(path) = hunter.current_path.clone() {
            if let Some(next_tile) = path.first() {
                let current_maze_coord =
                    chunk_tracking.chunk_coords_to_maze_coords(&hunter.chunk_coords);

                let intended_movement = IVec2::new(
                    next_tile.0 as i32 - current_maze_coord.0 as i32,
                    next_tile.1 as i32 - current_maze_coord.1 as i32,
                );

                let target_chunk_coords = chunk_tracking.calculate_resulting_coords_from_movement(
                    &hunter.chunk_coords,
                    &intended_movement,
                );

                if intended_movement.x > 0 {
                    hunter.facing = FacingDirection::East;
                } else if intended_movement.x < 0 {
                    hunter.facing = FacingDirection::West;
                } else if intended_movement.y > 0 {
                    hunter.facing = FacingDirection::North;
                } else if intended_movement.y < 0 {
                    hunter.facing = FacingDirection::South;
                }

                hunter.chunk_coords = target_chunk_coords;
                hunter.between_tiles = true;
                if path.len() == 1 {
                    hunter.current_path = None;
                } else {
                    hunter.current_path = Some(path[1..].to_vec());
                }
                transform.rotation = hunter.facing.as_quat();
            }
        }
    }
}
