use bevy::prelude::*;
use bevy_ecs_tilemap::map::TilemapType;

use super::ChunkTracking;
use crate::{
    dungeon::Dungeon,
    pedestal::{PedestalSpawnEvent, RuneSpawnEvent},
    player::Player,
    AppState,
};

pub struct ChunksPlugin;

impl Plugin for ChunksPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (spawn_chunks_around_player, despawn_out_of_range_chunks)
                .run_if(in_state(AppState::Running)),
        );
    }
}

pub fn spawn_chunks_around_player(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    player_query: Query<&Transform, With<Player>>,
    mut chunk_tracking: ResMut<ChunkTracking>,
    dungeon: Res<Dungeon>,
    mut pedestal_spawn_events: EventWriter<PedestalSpawnEvent>,
    mut rune_spawn_events: EventWriter<RuneSpawnEvent>,
) {
    for transform in player_query.iter() {
        let pos = transform.translation.xy();
        chunk_tracking.spawn_chunk_around_position(
            &mut commands,
            &asset_server,
            &dungeon,
            &pos,
            &mut pedestal_spawn_events,
            &mut rune_spawn_events,
        );
    }
}

pub fn despawn_out_of_range_chunks(
    mut commands: Commands,
    player_query: Query<&Transform, With<Player>>,
    chunks_query: Query<(Entity, &Transform), With<TilemapType>>,
    mut chunk_tracking: ResMut<ChunkTracking>,
) {
    for transform in player_query.iter() {
        let pos = transform.translation.xy();
        chunk_tracking.despawn_chunks_out_of_range_of_position(&mut commands, &chunks_query, &pos);
    }
}
