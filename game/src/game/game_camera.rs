use bevy::prelude::*;

use crate::{game::player_control::KeyAction, player::CameraFocalRect, AppState, CameraState};

use super::ControlState;

pub struct GameCameraPlugin;

impl Plugin for GameCameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup_camera).add_systems(
            Update,
            (
                toggle_camera_attach,
                move_camera.run_if(in_state(CameraState::DetachedCamera)),
            )
                .run_if(in_state(AppState::Running)),
        );
    }
}

fn setup_camera(mut commands: Commands) {
    commands.spawn((
        {
            let mut cam = Camera2dBundle::default();
            cam.projection.scale = 0.5;
            cam
        },
        CameraFocalRect {
            size: Vec2::new(16. * 10., 9. * 10.),
        },
    ));
}

fn move_camera(
    time: Res<Time>,
    controls: Res<ControlState>,
    mut query: Query<(&mut Transform, &mut OrthographicProjection), With<Camera2d>>,
) {
    for (mut transform, mut ortho) in query.iter_mut() {
        let mut translation = Vec2::ZERO;
        if controls.0.state[KeyAction::MoveUp].pressed() {
            translation.y += 1.;
        }
        if controls.0.state[KeyAction::MoveDown].pressed() {
            translation.y -= 1.;
        }
        if controls.0.state[KeyAction::MoveLeft].pressed() {
            translation.x -= 1.;
        }
        if controls.0.state[KeyAction::MoveRight].pressed() {
            translation.x += 1.;
        }

        if controls.0.state[KeyAction::ZoomIn].pressed() {
            ortho.scale -= 0.1;
        }
        if controls.0.state[KeyAction::ZoomOut].pressed() {
            ortho.scale += 0.1;
        }

        if ortho.scale < 0.5 {
            ortho.scale = 0.5;
        }

        transform.translation += translation.extend(0.) * time.delta_seconds() * 500.;
    }
}

fn toggle_camera_attach(
    current_state: Res<State<CameraState>>,
    mut state: ResMut<NextState<CameraState>>,
    controls: Res<ControlState>,
) {
    if controls.0.state[KeyAction::ToggleCamera].just_pressed() {
        match current_state.get() {
            CameraState::FollowPlayer => state.set(CameraState::DetachedCamera),
            CameraState::DetachedCamera => state.set(CameraState::FollowPlayer),
        }
    }
}
