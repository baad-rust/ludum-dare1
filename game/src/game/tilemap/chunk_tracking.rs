use bevy::reflect::TypePath;
use bevy::{prelude::*, utils::HashSet};
use bevy_ecs_tilemap::prelude::*;
use serde::{Deserialize, Serialize};

use crate::dungeon::Dungeon;
use crate::pedestal::{PedestalSpawnEvent, RuneSpawnEvent};

use super::generate_chunk;

#[derive(Debug, Clone, Copy)]
pub struct ChunkCoords {
    pub chunk_coord: IVec2,
    pub chunk_relative_coord: UVec2,
    pub tile_relative_pos: Vec2,
}

#[derive(Debug, Resource)]
pub struct ChunkTracking {
    tileset_name: String,
    tile_world_size: TilemapTileSize,
    chunk_tile_dimensions: UVec2,
    chunk_render_dimensions: UVec2,
    chunks_count_around_player: UVec2,
    chunk_despawn_square_distance: f32,
    tile_roles_by_image_index: Vec<TileRole>,
    active_chunks: HashSet<IVec2>,
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum TileRole {
    WallNorthFacing,
    WallEastFacing,
    WallSouthFacing,
    WallWestFacing,
    WallNorthSouthFacing,
    WallEastWestFacing,
    WallNorthEastSouthWestFacing,
    WallNorthWestSouthFacing,
    WallNorthEastSouthFacing,
    WallNorthEastWestFacing,
    WallSouthEastWestFacing,
    WallNorthWestOuterCorner,
    WallNorthEastOuterCorner,
    WallSouthWestOuterCorner,
    WallSouthEastOuterCorner,
    WallNorthWestInnerCorner,
    WallNorthEastInnerCorner,
    WallSouthWestInnerCorner,
    WallSouthEastInnerCorner,
    Floor,
    Void,
}

#[derive(Debug, Serialize, Deserialize, TypePath, Asset)]
pub struct TilemapMetadata {
    pub tile_image_resolution: Vec2,
    pub tiles_per_chunk_dimension: u32,
    pub chunk_despawn_distance_in_chunks: u32,
    pub tile_roles_by_image_index: Vec<TileRole>,
}

#[derive(Component)]
pub enum Traversability {
    Impassable,
    Passable,
}

impl Default for ChunkCoords {
    fn default() -> Self {
        Self {
            chunk_coord: IVec2::ZERO,
            chunk_relative_coord: UVec2::ZERO,
            tile_relative_pos: Vec2::ZERO,
        }
    }
}

impl PartialEq for ChunkCoords {
    fn eq(&self, other: &Self) -> bool {
        self.chunk_coord == other.chunk_coord
            && self.chunk_relative_coord == other.chunk_relative_coord
    }
}

impl Eq for ChunkCoords {}

impl ChunkTracking {
    pub fn new(tileset_name: &str, metadata: &TilemapMetadata) -> Self {
        let chunk_despawn_distance = metadata.chunk_despawn_distance_in_chunks as f32
            * metadata.tiles_per_chunk_dimension as f32
            * metadata
                .tile_image_resolution
                .x
                .max(metadata.tile_image_resolution.y);
        Self {
            tileset_name: tileset_name.to_string(),
            tile_world_size: metadata.tile_image_resolution.into(),
            chunk_tile_dimensions: UVec2::new(
                metadata.tiles_per_chunk_dimension,
                metadata.tiles_per_chunk_dimension,
            ),
            chunk_render_dimensions: metadata.tiles_per_chunk_dimension * UVec2::new(2, 2),
            chunks_count_around_player: UVec2::new(
                metadata.chunk_despawn_distance_in_chunks / 2,
                metadata.chunk_despawn_distance_in_chunks / 2,
            ),
            chunk_despawn_square_distance: chunk_despawn_distance * chunk_despawn_distance,
            tile_roles_by_image_index: metadata.tile_roles_by_image_index.clone(),
            active_chunks: HashSet::default(),
        }
    }

    pub fn raw_tile_pos_to_chunk_coord(&self, raw_tile_pos: &UVec2) -> ChunkCoords {
        let chunk_coord = *raw_tile_pos / self.chunk_tile_dimensions;
        let chunk_relative_coord = *raw_tile_pos - (chunk_coord * self.chunk_tile_dimensions);
        ChunkCoords {
            chunk_coord: chunk_coord.as_ivec2(),
            chunk_relative_coord,
            tile_relative_pos: Vec2::ZERO,
        }
    }

    pub fn world_pos_to_chunk_pos(&self, world_pos: &Vec2) -> IVec2 {
        let pos = world_pos.as_ivec2();
        let chunk_size: IVec2 = IVec2::new(
            self.chunk_tile_dimensions.x as i32,
            self.chunk_tile_dimensions.y as i32,
        );
        let tile_size: IVec2 =
            IVec2::new(self.tile_world_size.x as i32, self.tile_world_size.y as i32);
        pos / (chunk_size * tile_size)
    }

    pub fn chunk_pos_to_world_pos(&self, chunk_pos: IVec2) -> Vec2 {
        Vec2::new(
            chunk_pos.x as f32 * self.chunk_tile_dimensions.x as f32 * self.tile_world_size.x,
            chunk_pos.y as f32 * self.chunk_tile_dimensions.y as f32 * self.tile_world_size.y,
        )
    }

    fn spawn_chunk(
        &self,
        commands: &mut Commands,
        asset_server: &AssetServer,
        dungeon: &Dungeon,
        chunk_pos: IVec2,
        pedestal_spawn_events: &mut EventWriter<PedestalSpawnEvent>,
        rune_spawn_events: &mut EventWriter<RuneSpawnEvent>,
    ) {
        let tilemap_entity = commands.spawn_empty().id();
        let mut tile_storage = TileStorage::empty(self.chunk_tile_dimensions.into());

        generate_chunk(
            tilemap_entity,
            commands,
            &mut tile_storage,
            chunk_pos * self.chunk_tile_dimensions.as_ivec2(),
            self.chunk_tile_dimensions,
            dungeon,
            &self.tile_roles_by_image_index,
            pedestal_spawn_events,
            rune_spawn_events,
        );

        let transform =
            Transform::from_translation(self.chunk_pos_to_world_pos(chunk_pos).extend(0.));

        let texture_handle: Handle<Image> =
            asset_server.load(format!("tilesets/{}.png", self.tileset_name));
        commands.entity(tilemap_entity).insert(TilemapBundle {
            grid_size: self.tile_world_size.into(),
            size: self.chunk_tile_dimensions.into(),
            storage: tile_storage,
            texture: TilemapTexture::Single(texture_handle),
            tile_size: self.tile_world_size,
            transform,
            render_settings: TilemapRenderSettings {
                render_chunk_size: self.chunk_render_dimensions,
                ..Default::default()
            },
            ..Default::default()
        });
    }

    pub fn spawn_chunk_around_position(
        &mut self,
        commands: &mut Commands,
        asset_server: &AssetServer,
        dungeon: &Dungeon,
        pos: &Vec2,
        pedestal_spawn_events: &mut EventWriter<PedestalSpawnEvent>,
        rune_spawn_events: &mut EventWriter<RuneSpawnEvent>,
    ) {
        let chunk_pos = self.world_pos_to_chunk_pos(pos);
        for y in (chunk_pos.y - self.chunks_count_around_player.y as i32)
            ..(chunk_pos.y + self.chunks_count_around_player.y as i32)
        {
            for x in (chunk_pos.x - self.chunks_count_around_player.x as i32)
                ..(chunk_pos.x + self.chunks_count_around_player.x as i32)
            {
                let chunk_pos = IVec2::new(x, y);
                if !self.active_chunks.contains(&chunk_pos) {
                    self.active_chunks.insert(chunk_pos);
                    self.spawn_chunk(
                        commands,
                        asset_server,
                        dungeon,
                        chunk_pos,
                        pedestal_spawn_events,
                        rune_spawn_events,
                    );
                }
            }
        }
    }

    pub fn despawn_chunks_out_of_range_of_position(
        &mut self,
        commands: &mut Commands,
        chunks: &Query<(Entity, &Transform), With<TilemapType>>,
        pos: &Vec2,
    ) {
        for (entity, transform) in chunks.iter() {
            let chunk_world_pos = transform.translation.xy();
            let distance = pos.distance_squared(chunk_world_pos);
            if distance > self.chunk_despawn_square_distance {
                let x = (chunk_world_pos.x
                    / (self.chunk_tile_dimensions.x as f32 * self.tile_world_size.x))
                    .floor() as i32;
                let y = (chunk_world_pos.y
                    / (self.chunk_tile_dimensions.y as f32 * self.tile_world_size.y))
                    .floor() as i32;
                self.active_chunks.remove(&IVec2::new(x, y));
                commands.entity(entity).despawn_recursive();
            }
        }
    }

    fn calculate_position_relative_to_chunk_center(
        &self,
        chunk_relative_coord: &UVec2,
        tile_relative_pos: &Vec2,
    ) -> Vec2 {
        // let chunk_world_size = Vec2::new(
        //     self.chunk_tile_dimensions.x as f32 * self.tile_world_size.x,
        //     self.chunk_tile_dimensions.y as f32 * self.tile_world_size.y,
        // );
        Vec2::new(
            chunk_relative_coord.x as f32 * self.tile_world_size.x + tile_relative_pos.x,
            chunk_relative_coord.y as f32 * self.tile_world_size.y + tile_relative_pos.y,
        )
        // chunk_relative_world_pos - (chunk_world_size / 2.)
    }

    pub fn calculate_world_pos(&self, chunk_coords: &ChunkCoords) -> Vec2 {
        let world_pos_relative_to_chunk_center = self.calculate_position_relative_to_chunk_center(
            &chunk_coords.chunk_relative_coord,
            &chunk_coords.tile_relative_pos,
        );
        let chunk_center_world_pos = Vec2::new(
            chunk_coords.chunk_coord.x as f32
                * self.chunk_tile_dimensions.x as f32
                * self.tile_world_size.x,
            chunk_coords.chunk_coord.y as f32
                * self.chunk_tile_dimensions.y as f32
                * self.tile_world_size.y,
        );

        chunk_center_world_pos + world_pos_relative_to_chunk_center
    }

    pub fn calculate_resulting_coords_from_movement(
        &self,
        starting_chunk_coords: &ChunkCoords,
        movement: &IVec2,
    ) -> ChunkCoords {
        let mut new_chunk_relative_coord = IVec2::new(
            starting_chunk_coords.chunk_relative_coord.x as i32 + movement.x,
            starting_chunk_coords.chunk_relative_coord.y as i32 + movement.y,
        );

        // if negative, move into another chunk
        // if greater than the tile dimensions of a chunk, move into another chunk
        // if between, stay in the same chunk
        let mut new_chunk_coord = starting_chunk_coords.chunk_coord;
        if new_chunk_relative_coord.x < 0 {
            new_chunk_coord.x -= 1;
            new_chunk_relative_coord.x += self.chunk_tile_dimensions.x as i32;
        } else if new_chunk_relative_coord.x >= self.chunk_tile_dimensions.x as i32 {
            new_chunk_coord.x += 1;
            new_chunk_relative_coord.x -= self.chunk_tile_dimensions.x as i32;
        }

        if new_chunk_relative_coord.y < 0 {
            new_chunk_coord.y -= 1;
            new_chunk_relative_coord.y += self.chunk_tile_dimensions.y as i32;
        } else if new_chunk_relative_coord.y >= self.chunk_tile_dimensions.y as i32 {
            new_chunk_coord.y += 1;
            new_chunk_relative_coord.y -= self.chunk_tile_dimensions.y as i32;
        }

        let new_tile_relative_pos = Vec2::new(
            starting_chunk_coords.tile_relative_pos.x - movement.x as f32 * self.tile_world_size.x,
            starting_chunk_coords.tile_relative_pos.y - movement.y as f32 * self.tile_world_size.y,
        );
        ChunkCoords {
            chunk_coord: new_chunk_coord,
            chunk_relative_coord: new_chunk_relative_coord.as_uvec2(),
            tile_relative_pos: new_tile_relative_pos,
        }
    }

    pub fn chunk_coords_to_maze_coords(&self, chunk_coords: &ChunkCoords) -> (usize, usize) {
        let chunk_tile_coords = chunk_coords.chunk_coord * self.chunk_tile_dimensions.as_ivec2()
            + chunk_coords.chunk_relative_coord.as_ivec2();
        (chunk_tile_coords.x as usize, chunk_tile_coords.y as usize)
    }

    pub fn maze_coords_to_chunk_coords(&self, maze_coords: &(usize, usize)) -> ChunkCoords {
        let maze_coords = UVec2::new(maze_coords.0 as u32, maze_coords.1 as u32);
        let chunk_coord = IVec2::new(
            maze_coords.x as i32 / self.chunk_tile_dimensions.x as i32,
            maze_coords.y as i32 / self.chunk_tile_dimensions.y as i32,
        );
        let chunk_relative_coord = maze_coords % self.chunk_tile_dimensions;
        ChunkCoords {
            chunk_coord,
            chunk_relative_coord,
            tile_relative_pos: Vec2::ZERO,
        }
    }
}

impl Default for ChunkTracking {
    fn default() -> Self {
        ChunkTracking::new("tiles", &TilemapMetadata::default())
    }
}

impl Default for TilemapMetadata {
    fn default() -> Self {
        Self {
            tile_image_resolution: Vec2::new(16., 16.),
            tiles_per_chunk_dimension: 4,
            chunk_despawn_distance_in_chunks: 12,
            tile_roles_by_image_index: vec![
                TileRole::Floor,
                TileRole::Floor,
                TileRole::Floor,
                TileRole::Floor,
                TileRole::Floor,
                TileRole::Floor,
            ],
        }
    }
}
