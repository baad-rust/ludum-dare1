use bevy::prelude::*;
use bevy_ecs_tilemap::{
    map::TilemapId,
    tiles::{TileBundle, TilePos, TileStorage, TileTextureIndex},
};
use maze_gen::Cell;

use crate::{
    dungeon::Dungeon,
    hunter::HunterSpawnMarker,
    pedestal::{PedestalSpawnEvent, RuneSpawnEvent},
    player::SpawnPlayer,
};

use super::{TileRole, Traversability};

fn select_wall_type(
    north_neighbour: Option<Cell>,
    south_neighbour: Option<Cell>,
    east_neighbour: Option<Cell>,
    west_neighbour: Option<Cell>,
) -> TileRole {
    match (
        north_neighbour,
        south_neighbour,
        east_neighbour,
        west_neighbour,
    ) {
        (Some(Cell::Wall), Some(Cell::Wall), Some(Cell::Wall), Some(Cell::Wall)) => TileRole::Void,
        (None, Some(south_cell), Some(east_cell), Some(west_cell)) => {
            match (south_cell, east_cell, west_cell) {
                (Cell::Wall, Cell::Wall, Cell::Wall) => TileRole::Void,
                (_, Cell::Wall, Cell::Wall) => TileRole::WallSouthFacing,
                (Cell::Wall, _, Cell::Wall) => TileRole::WallEastFacing,
                (Cell::Wall, Cell::Wall, _) => TileRole::WallWestFacing,
                (Cell::Wall, _, _) => TileRole::WallEastWestFacing,
                (_, Cell::Wall, _) => TileRole::WallNorthEastInnerCorner,
                (_, _, Cell::Wall) => TileRole::WallNorthWestInnerCorner,
                (_, _, _) => TileRole::WallSouthEastWestFacing,
            }
        }
        (Some(north_cell), None, Some(east_cell), Some(west_cell)) => {
            match (north_cell, east_cell, west_cell) {
                (Cell::Wall, Cell::Wall, Cell::Wall) => TileRole::Void,
                (_, Cell::Wall, Cell::Wall) => TileRole::WallNorthFacing,
                (Cell::Wall, _, Cell::Wall) => TileRole::WallEastFacing,
                (Cell::Wall, Cell::Wall, _) => TileRole::WallWestFacing,
                (Cell::Wall, _, _) => TileRole::WallEastWestFacing,
                (_, Cell::Wall, _) => TileRole::WallSouthEastInnerCorner,
                (_, _, Cell::Wall) => TileRole::WallSouthWestInnerCorner,
                (_, _, _) => TileRole::WallNorthEastWestFacing,
            }
        }
        (Some(north_cell), Some(south_cell), None, Some(west_cell)) => {
            match (north_cell, south_cell, west_cell) {
                (Cell::Wall, Cell::Wall, Cell::Wall) => TileRole::Void,
                (_, Cell::Wall, Cell::Wall) => TileRole::WallNorthFacing,
                (Cell::Wall, _, Cell::Wall) => TileRole::WallSouthFacing,
                (Cell::Wall, Cell::Wall, _) => TileRole::WallWestFacing,
                (Cell::Wall, _, _) => TileRole::WallSouthEastInnerCorner,
                (_, Cell::Wall, _) => TileRole::WallNorthWestInnerCorner,
                (_, _, Cell::Wall) => TileRole::WallNorthSouthFacing,
                (_, _, _) => TileRole::WallNorthWestSouthFacing,
            }
        }
        (Some(north_cell), Some(south_cell), Some(east_cell), None) => {
            match (north_cell, south_cell, east_cell) {
                (Cell::Wall, Cell::Wall, Cell::Wall) => TileRole::Void,
                (_, Cell::Wall, Cell::Wall) => TileRole::WallNorthFacing,
                (Cell::Wall, _, Cell::Wall) => TileRole::WallSouthFacing,
                (Cell::Wall, Cell::Wall, _) => TileRole::WallEastFacing,
                (Cell::Wall, _, _) => TileRole::WallSouthEastInnerCorner,
                (_, Cell::Wall, _) => TileRole::WallNorthEastInnerCorner,
                (_, _, Cell::Wall) => TileRole::WallNorthSouthFacing,
                (_, _, _) => TileRole::WallNorthEastSouthFacing,
            }
        }
        (None, Some(south_cell), None, Some(west_cell)) => match (south_cell, west_cell) {
            (Cell::Wall, Cell::Wall) => TileRole::Void,
            (Cell::Wall, _) => TileRole::WallWestFacing,
            (_, Cell::Wall) => TileRole::WallSouthFacing,
            (_, _) => TileRole::WallSouthWestInnerCorner,
        },
        (None, Some(south_cell), Some(east_cell), None) => match (south_cell, east_cell) {
            (Cell::Wall, Cell::Wall) => TileRole::Void,
            (Cell::Wall, _) => TileRole::WallEastFacing,
            (_, Cell::Wall) => TileRole::WallSouthFacing,
            (_, _) => TileRole::WallSouthEastInnerCorner,
        },
        (Some(north_cell), None, None, Some(west_cell)) => match (north_cell, west_cell) {
            (Cell::Wall, Cell::Wall) => TileRole::Void,
            (Cell::Wall, _) => TileRole::WallWestFacing,
            (_, Cell::Wall) => TileRole::WallNorthFacing,
            (_, _) => TileRole::WallNorthWestInnerCorner,
        },
        (Some(north_cell), None, Some(east_cell), None) => match (north_cell, east_cell) {
            (Cell::Wall, Cell::Wall) => TileRole::Void,
            (Cell::Wall, _) => TileRole::WallEastFacing,
            (_, Cell::Wall) => TileRole::WallNorthFacing,
            (_, _) => TileRole::WallNorthEastInnerCorner,
        },
        (Some(_), Some(Cell::Wall), Some(Cell::Wall), Some(Cell::Wall)) => {
            TileRole::WallNorthFacing
        }
        (Some(Cell::Wall), Some(_), Some(Cell::Wall), Some(Cell::Wall)) => {
            TileRole::WallSouthFacing
        }
        (Some(Cell::Wall), Some(Cell::Wall), Some(_), Some(Cell::Wall)) => TileRole::WallEastFacing,
        (Some(Cell::Wall), Some(Cell::Wall), Some(Cell::Wall), Some(_)) => TileRole::WallWestFacing,
        (Some(Cell::Wall), Some(Cell::Wall), Some(_), Some(_)) => TileRole::WallEastWestFacing,
        (Some(_), Some(_), Some(Cell::Wall), Some(Cell::Wall)) => TileRole::WallNorthSouthFacing,
        (Some(Cell::Wall), Some(_), Some(Cell::Wall), Some(_)) => {
            TileRole::WallSouthWestInnerCorner
        }
        (Some(Cell::Wall), Some(_), Some(_), Some(Cell::Wall)) => {
            TileRole::WallSouthEastInnerCorner
        }
        (Some(_), Some(Cell::Wall), Some(Cell::Wall), Some(_)) => {
            TileRole::WallNorthWestInnerCorner
        }
        (Some(_), Some(Cell::Wall), Some(_), Some(Cell::Wall)) => {
            TileRole::WallNorthEastInnerCorner
        }
        (Some(Cell::Wall), Some(_), Some(_), Some(_)) => TileRole::WallSouthEastWestFacing,
        (Some(_), Some(Cell::Wall), Some(_), Some(_)) => TileRole::WallNorthEastWestFacing,
        (Some(_), Some(_), Some(Cell::Wall), Some(_)) => TileRole::WallNorthWestSouthFacing,
        (Some(_), Some(_), Some(_), Some(Cell::Wall)) => TileRole::WallNorthEastSouthFacing,
        _ => TileRole::Void,
    }
}

pub fn generate_chunk(
    tilemap_entity: Entity,
    commands: &mut Commands,
    tile_storage: &mut TileStorage,
    world_pos: IVec2,
    chunk_size: UVec2,
    dungeon: &Dungeon,
    tile_roles_map: &[TileRole],
    pedestal_spawn_events: &mut EventWriter<PedestalSpawnEvent>,
    rune_spawn_events: &mut EventWriter<RuneSpawnEvent>,
) {
    let (spawn_x, spawn_y) = dungeon.player_spawn;
    let maze = &dungeon.maze;

    for y in 0..chunk_size.y {
        for x in 0..chunk_size.x {
            let (wx, wy) = (x as i32 + world_pos.x, y as i32 + world_pos.y);
            if wx >= 0 && wx < maze.width() as i32 && wy >= 0 && wy < maze.height() as i32 {
                let wx = wx as usize;
                let wy = wy as usize;
                let tile = maze.get(wx, wy);
                let tile_role = match tile {
                    maze_gen::Cell::Wall => {
                        let south_cell = if (wy as i32) - 1 >= 0 {
                            Some(maze.get(wx, wy - 1))
                        } else {
                            None
                        };
                        let north_cell = if (wy as i32) + 1 < maze.height() as i32 {
                            Some(maze.get(wx, wy + 1))
                        } else {
                            None
                        };
                        let west_cell = if (wx as i32) - 1 >= 0 {
                            Some(maze.get(wx - 1, wy))
                        } else {
                            None
                        };
                        let east_cell = if (wx as i32) + 1 < maze.width() as i32 {
                            Some(maze.get(wx + 1, wy))
                        } else {
                            None
                        };

                        select_wall_type(north_cell, south_cell, east_cell, west_cell)
                    }
                    maze_gen::Cell::Corridor(_) => TileRole::Floor,
                    maze_gen::Cell::Room(_) => TileRole::Floor,
                    maze_gen::Cell::Door(_) => TileRole::Floor,
                };
                let tile_id = tile_roles_map
                    .iter()
                    .position(|&r| r == tile_role)
                    .unwrap_or(0) as u32;

                let tile_bundle = TileBundle {
                    position: TilePos::new(x, y),
                    tilemap_id: TilemapId(tilemap_entity),
                    texture_index: TileTextureIndex(tile_id),
                    ..Default::default()
                };

                let tile_entity = {
                    let mut tile_entity = commands.spawn(tile_bundle);

                    if spawn_x == wx && spawn_y == wy {
                        tile_entity.insert(SpawnPlayer);
                    }
                    tile_entity.insert(if tile.is_floor() {
                        Traversability::Passable
                    } else {
                        Traversability::Impassable
                    });

                    if let Cell::Room(room_id) = tile {
                        if room_id != dungeon.player_spawn_room_id {
                            // somehow a tile can be both a room and a corridor while storing a corridor ID
                            if let Some(room) = maze.get_room(room_id) {
                                let room_centre =
                                    (room.x + room.width / 2, room.y + room.height / 2);

                                let distance_from_spawn =
                                    (Vec2::new(room_centre.0 as f32, room_centre.1 as f32)
                                        - Vec2::new(spawn_x as f32, spawn_y as f32))
                                    .normalize()
                                    .abs()
                                    .max_element();

                                if room_centre == (wx, wy) {
                                    tile_entity.insert(HunterSpawnMarker::new(distance_from_spawn));
                                }
                            }
                        }
                    }

                    tile_entity.id()
                };

                commands.entity(tilemap_entity).add_child(tile_entity);
                tile_storage.set(&TilePos::new(x, y), tile_entity);

                if let Some((x, y, rune)) = dungeon
                    .rune_spawns
                    .iter()
                    .find(|(x, y, _)| *x == wx && *y == wy)
                {
                    rune_spawn_events.send(RuneSpawnEvent {
                        maze_coords: (*x, *y),
                        rune: *rune,
                    });
                }
                if let Some((x, y, rune)) = dungeon
                    .pedestal_spawns
                    .iter()
                    .find(|(x, y, _)| *x == wx && *y == wy)
                {
                    pedestal_spawn_events.send(PedestalSpawnEvent {
                        maze_coords: (*x, *y),
                        rune: *rune,
                    });
                }
            }
        }
    }
}
