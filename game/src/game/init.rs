use bevy::prelude::*;
use bevy_common_assets::ron::RonAssetPlugin;

use crate::{
    dungeon::Dungeon,
    hunter::HunterSpawnEvent,
    pedestal::{PedestalSpawnEvent, RuneSpawnEvent},
    player::PlayerSpawnEvent,
    AppState, LoadingCheckList, TilemapMetadata, TilemapMetadataHandle,
};

use super::{random, ChunkTracking, Rand};

pub struct GameInitPlugin;

impl Plugin for GameInitPlugin {
    fn build(&self, app: &mut App) {
        let seed = Some(1000);
        let rng = random::Rand::new(seed);

        app.init_state::<AppState>()
            .insert_resource(rng)
            .insert_resource(LoadingCheckList::default())
            .add_plugins(RonAssetPlugin::<TilemapMetadata>::new(&["tiledata.ron"]))
            .add_systems(Startup, load_tile_sets)
            .add_systems(
                Update,
                (generate_maze, spawn_initial_chunks, level_init)
                    .chain()
                    .run_if(in_state(AppState::LevelInit)),
            )
            .add_systems(
                Update,
                (init_game_data, check_loading_checklist)
                    .chain()
                    .run_if(in_state(AppState::Loading)),
            );
    }
}

fn init_game_data(
    mut commands: Commands,
    tile_metadata_handle: Res<TilemapMetadataHandle>,
    mut metadata: ResMut<Assets<TilemapMetadata>>,
    mut check_list: ResMut<LoadingCheckList>,
) {
    if let Some(tile_metadata) = metadata.remove(tile_metadata_handle.handle.id()) {
        commands.insert_resource(ChunkTracking::new(
            &tile_metadata_handle.tileset_name,
            &tile_metadata,
        ));
        check_list.tile_metadata = true;
    }
}

fn check_loading_checklist(
    mut state: ResMut<NextState<AppState>>,
    check_list: Res<LoadingCheckList>,
) {
    if check_list.all_complete() {
        state.set(AppState::LevelInit);
    }
}

fn load_tile_sets(mut commands: Commands, asset_server: Res<AssetServer>) {
    let tileset_name = "simple_walls";
    let tile_metadata = TilemapMetadataHandle {
        handle: asset_server.load(format!("tilesets/{tileset_name}.tiledata.ron")),
        tileset_name: String::from(tileset_name),
    };
    commands.insert_resource(tile_metadata);
}

fn generate_maze(mut commands: Commands, rng: Res<Rand>) {
    let dungeon = Dungeon::new(rng.clone());
    commands.insert_resource(dungeon);
}

fn spawn_initial_chunks(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    dungeon: Res<Dungeon>,
    mut chunk_tracking: ResMut<ChunkTracking>,
    mut player_spawn_events: EventWriter<PlayerSpawnEvent>,
    mut hunter_spawn_events: EventWriter<HunterSpawnEvent>,
    mut pedestal_spawn_events: EventWriter<PedestalSpawnEvent>,
    mut rune_spawn_evetns: EventWriter<RuneSpawnEvent>,
) {
    let (spawn_x, spawn_y) = dungeon.player_spawn;
    let spawn_point_chunk_coords =
        chunk_tracking.raw_tile_pos_to_chunk_coord(&UVec2::new(spawn_x as u32, spawn_y as u32));
    let world_pos = chunk_tracking.calculate_world_pos(&spawn_point_chunk_coords);
    chunk_tracking.spawn_chunk_around_position(
        &mut commands,
        &asset_server,
        &dungeon,
        &world_pos,
        &mut pedestal_spawn_events,
        &mut rune_spawn_evetns,
    );
    player_spawn_events.send(PlayerSpawnEvent {
        spawn_coords: spawn_point_chunk_coords,
    });
    hunter_spawn_events.send(HunterSpawnEvent);
}

fn level_init(mut state: ResMut<NextState<AppState>>) {
    state.set(AppState::Running);
}
