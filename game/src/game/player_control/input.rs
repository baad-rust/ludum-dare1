use bevy::{
    input::gamepad::{GamepadConnection, GamepadEvent},
    prelude::*,
};
use bevy_common_assets::ron::RonAssetPlugin;
use enum_map::EnumMap;

use super::{AxisDirection, KeyAction, KeyBind, Keymapping, KeymappingHandle};
use crate::{AppState, LoadingCheckList};

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(RonAssetPlugin::<Keymapping>::new(&["keys.ron"]))
            .insert_resource(ControlState::default())
            .add_systems(Startup, load_key_mapping)
            .add_systems(
                Update,
                (
                    init_keymapping,
                    (gamepad_connected, update_controller_mappings)
                        .run_if(in_state(AppState::Running)),
                ),
            );
    }
}

#[derive(Default, Resource)]
pub struct ControlState(pub PlayerControls);

#[derive(Default, Clone, Copy)]
pub struct ButtonState {
    pub pressed: bool,
    pub state_changed_this_frame: bool,
}

#[derive(Default)]
pub struct PlayerControls {
    pub pad: Option<Gamepad>,
    pub state: EnumMap<KeyAction, ButtonState>,
}

fn init_keymapping(
    mut commands: Commands,
    key_mapping_handle: Res<KeymappingHandle>,
    mut key_mapping_asset: ResMut<Assets<Keymapping>>,
    mut check_list: ResMut<LoadingCheckList>,
) {
    if let Some(key_mapping) = key_mapping_asset.remove(key_mapping_handle.0.id()) {
        commands.insert_resource(key_mapping);
        check_list.key_mapping = true;
    }
}

fn load_key_mapping(mut commands: Commands, asset_server: Res<AssetServer>) {
    let key_mapping = KeymappingHandle(asset_server.load("keymapping.keys.ron"));
    commands.insert_resource(key_mapping);
}

impl ButtonState {
    pub fn pressed(&self) -> bool {
        self.pressed
    }

    pub fn just_pressed(&self) -> bool {
        self.pressed && self.state_changed_this_frame
    }

    pub fn _just_released(&self) -> bool {
        !self.pressed && self.state_changed_this_frame
    }
}

pub fn gamepad_connected(
    mut game_state: ResMut<ControlState>,
    mut gamepad_event: EventReader<GamepadEvent>,
) {
    for event in gamepad_event.read() {
        if let GamepadEvent::Connection(connection_event) = event {
            game_state.0.pad = match connection_event.connection {
                GamepadConnection::Connected(_) => Some(connection_event.gamepad),
                GamepadConnection::Disconnected => None,
            };
        }
    }
}

pub fn update_controller_mappings(
    mut game_state: ResMut<ControlState>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
    gamepad_buttons: Res<ButtonInput<GamepadButton>>,
    gamepad_axes: Res<Axis<GamepadAxis>>,
    key_mapping: Res<Keymapping>,
) {
    const GAMEPAD_AXIS_THRESHOLD: f32 = 0.5;

    let player_control = &mut game_state.0;
    fn write_button_state(
        keybind: &KeyBind,
        button_state: &mut ButtonState,
        keyboard_input: &Res<ButtonInput<KeyCode>>,
        gamepad_buttons: &Res<ButtonInput<GamepadButton>>,
        gamepad_axes: &Res<Axis<GamepadAxis>>,
        pad: Option<Gamepad>,
    ) {
        match keybind {
            KeyBind::Key(key_code) => {
                button_state.pressed |= keyboard_input.pressed(*key_code);
            }
            KeyBind::ControllerButton(pad_button) => {
                if let Some(pad) = pad {
                    button_state.pressed |= gamepad_buttons.pressed(GamepadButton {
                        gamepad: pad,
                        button_type: *pad_button,
                    });
                }
            }
            KeyBind::ControllerAxis((pad_axis, axis_direction)) => {
                if let Some(pad) = pad {
                    button_state.pressed |= gamepad_axes
                        .get(GamepadAxis {
                            gamepad: pad,
                            axis_type: *pad_axis,
                        })
                        .map_or(false, |v| match axis_direction {
                            AxisDirection::Positive => v > GAMEPAD_AXIS_THRESHOLD,
                            AxisDirection::Negative => v < -GAMEPAD_AXIS_THRESHOLD,
                        });
                }
            }
        }
    }

    let prev_control_state = player_control.state;
    let mut new_control_state = EnumMap::default();

    let pad = player_control.pad;

    for (key_action, key_bind) in &key_mapping.0 {
        let new_button_state = &mut new_control_state[key_action.clone()];
        write_button_state(
            &key_bind.primary,
            new_button_state,
            &keyboard_input,
            &gamepad_buttons,
            &gamepad_axes,
            pad,
        );

        write_button_state(
            &key_bind.secondary,
            new_button_state,
            &keyboard_input,
            &gamepad_buttons,
            &gamepad_axes,
            pad,
        );

        new_button_state.state_changed_this_frame =
            new_button_state.pressed != prev_control_state[key_action].pressed;

        player_control.state = new_control_state;
    }
}
