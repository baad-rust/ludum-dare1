use bevy::prelude::*;
use enum_map::{enum_map, Enum, EnumMap};
use serde::{Deserialize, Serialize};

#[derive(Resource, Asset, TypePath, Serialize, Deserialize)]
pub struct Keymapping(pub EnumMap<KeyAction, KeyBindConfig>);

#[derive(Resource, Default)]
pub struct KeymappingHandle(pub Handle<Keymapping>);

#[derive(Serialize, Deserialize)]
pub enum AxisDirection {
    Positive,
    Negative,
}

#[derive(Serialize, Deserialize)]
pub enum KeyBind {
    Key(KeyCode),
    ControllerButton(GamepadButtonType),
    ControllerAxis((GamepadAxisType, AxisDirection)),
}

#[derive(Serialize, Deserialize)]
pub struct KeyBindConfig {
    pub primary: KeyBind,
    pub secondary: KeyBind,
}

#[derive(Enum, Serialize, Deserialize, Clone)]
pub enum KeyAction {
    MoveUp,
    MoveDown,
    MoveLeft,
    MoveRight,
    ZoomIn,
    ZoomOut,
    ToggleCamera,
}

impl Default for Keymapping {
    fn default() -> Self {
        Self(enum_map! {
            KeyAction::MoveUp => KeyBindConfig {
                primary: KeyBind::Key(KeyCode::KeyW),
                secondary: KeyBind::ControllerAxis((GamepadAxisType::LeftStickY, AxisDirection::Positive)),
            },
            KeyAction::MoveDown => KeyBindConfig {
                primary: KeyBind::Key(KeyCode::KeyS),
                secondary: KeyBind::ControllerAxis((GamepadAxisType::LeftStickY, AxisDirection::Negative)),
            },
            KeyAction::MoveLeft => KeyBindConfig {
                primary: KeyBind::Key(KeyCode::KeyA),
                secondary: KeyBind::ControllerAxis((GamepadAxisType::LeftStickX, AxisDirection::Negative)),
            },
            KeyAction::MoveRight => KeyBindConfig {
                primary: KeyBind::Key(KeyCode::KeyD),
                secondary: KeyBind::ControllerAxis((GamepadAxisType::LeftStickX, AxisDirection::Positive)),
            },
            KeyAction::ZoomIn => KeyBindConfig {
                primary: KeyBind::Key(KeyCode::KeyZ),
                secondary: KeyBind::ControllerAxis((GamepadAxisType::RightStickY, AxisDirection::Positive)),
            },
            KeyAction::ZoomOut => KeyBindConfig {
                primary: KeyBind::Key(KeyCode::KeyX),
                secondary: KeyBind::ControllerAxis((GamepadAxisType::RightStickY, AxisDirection::Negative)),
            },
            KeyAction::ToggleCamera => KeyBindConfig {
                primary: KeyBind::Key(KeyCode::F1),
                secondary: KeyBind::ControllerButton(GamepadButtonType::Select),
            },
        })
    }
}
