mod chunks;
mod game_camera;
mod init;
mod player_control;
mod random;
mod tilemap;

use bevy::app::{PluginGroup, PluginGroupBuilder};
pub use chunks::*;
pub use game_camera::*;
pub use init::*;
pub use player_control::*;
pub use random::*;
pub use tilemap::*;

pub struct GamePlugins;

impl PluginGroup for GamePlugins {
    fn build(self) -> PluginGroupBuilder {
        PluginGroupBuilder::start::<Self>()
            .add(GameInitPlugin)
            .add(ChunksPlugin)
            .add(InputPlugin)
            .add(GameCameraPlugin)
    }
}
