use maze_gen::{Cell, Maze};
use pathfinding::prelude::astar;

pub type Path = Vec<(usize, usize)>;

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct Point {
    x: usize,
    y: usize,
}

impl Point {
    fn neighbours(&self, maze: &Maze) -> Vec<Point> {
        let mut neighbours = Vec::new();
        if self.x > 0 && maze.get(self.x - 1, self.y) != Cell::Wall {
            neighbours.push(Point {
                x: self.x - 1,
                y: self.y,
            });
        }
        if self.y > 0 && maze.get(self.x, self.y - 1) != Cell::Wall {
            neighbours.push(Point {
                x: self.x,
                y: self.y - 1,
            });
        }
        if self.x < maze.width() - 1 && maze.get(self.x + 1, self.y) != Cell::Wall {
            neighbours.push(Point {
                x: self.x + 1,
                y: self.y,
            });
        }
        if self.y < maze.height() - 1 && maze.get(self.x, self.y + 1) != Cell::Wall {
            neighbours.push(Point {
                x: self.x,
                y: self.y + 1,
            });
        }
        neighbours
    }

    fn manhattan_distance(&self, other: &Point) -> usize {
        let dx = if self.x > other.x {
            self.x - other.x
        } else {
            other.x - self.x
        };
        let dy = if self.y > other.y {
            self.y - other.y
        } else {
            other.y - self.y
        };
        dx + dy
    }
}

pub fn find_path(maze: &Maze, start: (usize, usize), end: (usize, usize)) -> Option<Path> {
    let start = Point {
        x: start.0,
        y: start.1,
    };
    let end = Point { x: end.0, y: end.1 };
    let path = astar(
        &start,
        |p| p.neighbours(maze).into_iter().map(|p| (p, 1)),
        |p| p.manhattan_distance(&end),
        |p| *p == end,
    );

    if let Some((path, _cost)) = path {
        Some(path.into_iter().map(|p| (p.x, p.y)).collect())
    } else {
        None
    }
}
