use bevy::prelude::*;
use enum_map::{enum_map, Enum, EnumMap};

use crate::{
    game::{ChunkCoords, ChunkTracking},
    player::Player,
};

#[derive(Resource, Default)]
pub struct PlayerInventory {
    pub runes: EnumMap<Rune, bool>,
}

#[derive(Event)]
pub struct PedestalSpawnEvent {
    pub maze_coords: (usize, usize),
    pub rune: Rune,
}

#[derive(Event)]
pub struct RuneSpawnEvent {
    pub maze_coords: (usize, usize),
    pub rune: Rune,
}

#[derive(Event)]
pub struct RuneDespawnEvent {
    pub rune: Rune,
}

#[derive(Debug, Enum, Clone, Copy, PartialEq, Eq)]
pub enum Rune {
    H,
    O,
    R,
    N,
}

#[derive(Component)]
pub struct Pedestal {
    pub chunk_coords: ChunkCoords,
    pub activation_distance: u32,
    pub activated: bool,
    pub assigned_rune: Rune,
}

#[derive(Component)]
pub struct RuneTag {
    pub chunk_coords: ChunkCoords,
    pub rune: Rune,
}

pub fn spawn_pedestals(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut events: EventReader<PedestalSpawnEvent>,
    chunk_tracking: Res<ChunkTracking>,
) {
    for event in events.read() {
        let chunk_coords = chunk_tracking.maze_coords_to_chunk_coords(&event.maze_coords);
        let world_pos = chunk_tracking.calculate_world_pos(&chunk_coords);
        commands
            .spawn(SpriteBundle {
                texture: asset_server.load("pedestal_unlit.png"),
                transform: Transform::from_translation(world_pos.extend(1.)),
                ..Default::default()
            })
            .insert(Pedestal {
                chunk_coords: chunk_coords,
                activation_distance: 5,
                activated: false,
                assigned_rune: event.rune,
            });
    }

    events.clear();
}

pub fn spawn_runes(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut events: EventReader<RuneSpawnEvent>,
    chunk_tracking: Res<ChunkTracking>,
) {
    let rune_sprites = enum_map! {
        Rune::H => "rune_h.png",
        Rune::O => "rune_o.png",
        Rune::R => "rune_r.png",
        Rune::N => "rune_n.png"
    };
    for event in events.read() {
        let chunk_coords = chunk_tracking.maze_coords_to_chunk_coords(&event.maze_coords);
        let world_pos = chunk_tracking.calculate_world_pos(&chunk_coords);
        commands
            .spawn(SpriteBundle {
                texture: asset_server.load(rune_sprites[event.rune]),
                transform: Transform::from_translation(world_pos.extend(1.)),
                ..Default::default()
            })
            .insert(RuneTag {
                chunk_coords,
                rune: event.rune,
            });
    }

    events.clear();
}

pub fn activate_pedestals(
    mut pedestal_query: Query<(&mut Pedestal, &mut Handle<Image>)>,
    player_query: Query<&Player>,
    asset_server: Res<AssetServer>,
    inventory: Res<PlayerInventory>,
    chunk_tracking: Res<ChunkTracking>,
) {
    for (mut pedestal, mut texture) in pedestal_query.iter_mut() {
        if inventory.runes[pedestal.assigned_rune] {
            for player in player_query.iter() {
                let (player_x, player_y) =
                    chunk_tracking.chunk_coords_to_maze_coords(&player.chunk_coords);
                let (pedestal_x, pedestal_y) =
                    chunk_tracking.chunk_coords_to_maze_coords(&pedestal.chunk_coords);
                let player_coords = IVec2::new(player_x as i32, player_y as i32);
                let pedestal_coords = IVec2::new(pedestal_x as i32, pedestal_y as i32);
                let distance = player_coords.distance_squared(pedestal_coords);

                let previously_active = pedestal.activated;
                pedestal.activated = distance
                    <= (pedestal.activation_distance * pedestal.activation_distance) as i32;

                if previously_active != pedestal.activated {
                    if pedestal.activated {
                        *texture = asset_server.load("pedestal_lit.png");
                    } else {
                        *texture = asset_server.load("pedestal_unlit.png");
                    }
                }
            }
        }
    }
}

pub fn check_for_rune_collection(
    rune_query: Query<&RuneTag>,
    player_query: Query<&Player>,
    mut inventory: ResMut<PlayerInventory>,
    mut despawn_event: EventWriter<RuneDespawnEvent>,
) {
    for rune_tag in rune_query.iter() {
        if let Some(player) = player_query.iter().next() {
            let collect = rune_tag.chunk_coords == player.chunk_coords;
            if collect {
                inventory.runes[rune_tag.rune] = true;
                despawn_event.send(RuneDespawnEvent {
                    rune: rune_tag.rune,
                });
            }
        }
    }
}

pub fn despawn_collected_runes(
    mut commands: Commands,
    mut despawn_events: EventReader<RuneDespawnEvent>,
    rune_query: Query<(Entity, &RuneTag)>,
) {
    for event in despawn_events.read() {
        for (entity, _) in rune_query.iter().filter(|(_, r)| r.rune == event.rune) {
            commands.entity(entity).despawn();
        }
    }
}
