default:
    just --list

dev:
    just game/dev

read:
    just game/read

run:
    just game/run

run-release:
    just game/run-release

run-final:
    just game/run-final

alias d := dev
alias rd := read
alias r := run
alias rr := run-release
alias rf := run-final

